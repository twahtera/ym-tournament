{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api where

import Servant

import Types

import Data.Time.Clock (UTCTime)

type SeriesAPI = "standings" :> Get '[JSON] [Fencer]
                 :<|> "competition" :> Capture "resultid" ResultId :> Get '[JSON] [Fencer]
                 :<|> "competition" :> ReqBody '[JSON] UTCTime :> Put '[JSON] (Maybe ResultId)
                 :<|> "competition" :> Capture "resultid" ResultId :> ReqBody '[JSON] [FencerId] :> Put '[JSON] ()
                 :<|> "fencer" :> Capture "FencerId" FencerId :> Get '[JSON] (Maybe Fencer)
                 :<|> "fencer" :> ReqBody '[JSON] NewFencer :> Put '[JSON] (Maybe Fencer)

