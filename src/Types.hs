{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Types where

import Data.Text (Text)
import GHC.Generics
import Data.Aeson.Types
import Database.PostgreSQL.Simple


-- type FencerId = Int
type FencerId = Int

data Fencer = Fencer {fencerId :: FencerId, fencerName :: Text, fencerPoints :: Integer}
            deriving (Show, Eq, Ord, Generic)

data NewFencer = NewFencer {newFencerName :: Text}
            deriving (Show, Eq, Ord, Generic)

-- newtype ResultId = ResultId Int
--     deriving (Show, Eq, Ord, Generic)

type ResultId = Int
type Result = [Fencer]

--instance FromRow ResultId
-- instance ToRow ResultId

instance ToJSON Fencer
instance FromJSON Fencer
instance FromRow Fencer
instance ToRow Fencer

instance ToJSON NewFencer
instance FromJSON NewFencer
instance FromRow NewFencer
instance ToRow NewFencer



