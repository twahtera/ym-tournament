{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Maybe

import Servant
import Network.Wai.Handler.Warp
import Servant

import Control.Monad.IO.Class (liftIO)

import Database.PostgreSQL.Simple
import Data.Time.Clock

import Types
import Api


points :: [a] -> [Integer]
points results = zipWith (+) ([3,2,1] ++ [0, 0..]) $ basepoints results
    where basepoints fs = reverse $ map fst $ zip [1..] fs

-- Test data
f1 = Fencer 1 "f1" 10
f2 = Fencer 2 "f2" 32
f3 = Fencer 3 "f3" 12
f4 = Fencer 4 "f4" 49
f5 = Fencer 5 "f5" 13

fencers = [f1, f2, f3, f4, f5]

-- Servant boilerplate
seriesAPI :: Proxy SeriesAPI
seriesAPI = Proxy

makeApp :: Connection -> Application
makeApp c = serve seriesAPI $ server c

-- Server definition
server :: Connection -> Server SeriesAPI
server c = getStandings c
           :<|> getResult c
           :<|> putCompetition c
           :<|> addResult c
           :<|> getFencer c
           :<|> putFencer c

-- Some handlers
getStandings :: Connection -> Handler [Fencer]
getStandings c = liftIO $ query_ c "select * from fencer \
                                   \order by points desc"
getResult :: Connection -> ResultId -> Handler [Fencer]
getResult c resId =
   liftIO $ query c "select f.name, f.points \
            \from fencer f, competition_results r\
            \where r.competition_id = ? AND f.id = r.fencer_id\
            \order by rank asc" $ Only resId

getFencer :: Connection -> ResultId -> Handler (Maybe Fencer)
getFencer c id = liftIO $ fmap listToMaybe $
    query c "select * \
            \from fencer f \
            \where f.id = ?" $ Only id

putFencer :: Connection -> NewFencer -> Handler (Maybe Fencer)
putFencer c newFencer = liftIO $ fmap listToMaybe $
        query c "insert into fencer (name, points) values (?, 0) \
                             \returning *" [newFencerName newFencer]


putCompetition :: Connection -> UTCTime -> Handler (Maybe  ResultId)
putCompetition c compTime = liftIO $ fmap (listToMaybe . (fmap fromOnly)) $
    query c "insert into competition (time) values (?) \
            \returning (id)" [compTime]

addResult :: Connection -> ResultId -> [FencerId] -> Handler ()
addResult c compId fIds = liftIO $
    let insertVals = map (\ (fId, rank) -> (compId, fId, rank)) (zip fIds ([1..] :: [Int]))
    in
        do executeMany c "insert into competition_results \
                         \(competition_id, fencer_id, rank) values (?, ?,?)" insertVals
           return ()





-- Run the server
main :: IO ()
main = do
    dbconnection <- connect defaultConnectInfo { connectDatabase = "ym_tournament"
                                              , connectUser = "ym_tournament"
                                              , connectPassword = "ym_tournament" }
    run 8081 $ makeApp dbconnection
    
