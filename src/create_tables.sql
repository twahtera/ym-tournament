-- Fencers
create table if not exists fencer
(id serial primary key,
 name text not null,
 points integer not null);

-- Competitions
create table if not exists competition
(id serial primary key,
 time timestamp);

-- Fencer rankings in finished competition
create table if not exists competition_results
(competition_id serial references competition not null,
 fencer_id serial references fencer not null,
 rank integer constraint positive_rank check (rank > 0), 
 unique (competition_id, rank),
 unique (competition_id, fencer_id));


